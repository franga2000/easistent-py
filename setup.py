#!/bin/env python3
from setuptools import setup

setup(
    name="easistent",
    version="0.5.0",
    description="Knjižnica in orodja za delo z sistemom eAsistent",
    long_description="Knjižnica in orodja za delo z sistemom eAsistent",
    url="http://github.com/franga2000/easistent-py",
    author="Miha Frangež",
    author_email="miha.frangez@gmail.com",
    license="GPLv2",
    packages=["easistent"],
    setup_requires="setuptools-pipfile",
    use_pipfile=True,
)
