import dataclasses
import re
from dataclasses import dataclass
from datetime import datetime
from typing import Optional, List

import lxml.html
import requests

from easistent import util

re_teden_id = re.compile(
    r"ednevnik-seznam_ur_teden-td-(?P<ura>.+)-(?P<datum>\d+-\d+-\d+)"
)
re_vrsta_class = re.compile(r".*ednevnik-seznam_ur_teden-td-(\w+).*")


@dataclass
class Ura:
    začetek: datetime
    konec: datetime

    id: Optional[int]
    vrsta: Optional[str]
    predmet: str
    predmet_polno: Optional[str]
    profesor: str
    učilnica: str

    def __getitem__(cls, x):
        return getattr(cls, x)


def get_ure(dom: lxml.html.HtmlElement) -> dict:
    ure = {}
    for el in dom.cssselect(".ednevnik-seznam_ur_teden-ura"):
        ura = (
            el.cssselect(".text14")[0]
            .text_content()
            .replace(" ", "")
            .replace(".ura", "")
        )
        if ura == "Časpopouku":
            ura = "Po"
        elif ura == "Predura":
            ura = "0"
        čas = el.cssselect(".text10")[0].text.split(" - ")
        ure[ura] = {"od": čas[0], "do": čas[1]}
    return ure


def get_urnik(dom: lxml.html.HtmlElement, ure: dict, id: bool = False) -> List[Ura]:
    """Prebere urnik iz lxml dokumenta"""
    urnik = []

    for el in dom.cssselect(
        ".ednevnik-seznam_ur_teden-td:not(.ednevnik-seznam_ur_teden-ura)"
    ):  # Vsa polja, ki niso seznam ur (levi stolpec)

        m = re_teden_id.match(el.attrib["id"])
        ura = Ura(
            začetek=util.preberi_čas(
                m.group("datum") + " " + ure[m.group("ura")]["od"]
            ),
            konec=util.preberi_čas(m.group("datum") + " " + ure[m.group("ura")]["do"]),
            id=None,
            vrsta="",
            predmet="",
            predmet_polno="",
            profesor="",
            učilnica="",
        )

        els = el.cssselect(".ednevnik-seznam_ur_teden-urnik")

        # Prosta ura
        if not els:
            continue

        for e in els:  # Vsaka ura (predmet) v eni uri (čas)
            # Naredi kopijo elementa (reši težave z dvojnimi urami)
            ura = dataclasses.replace(ura)

            # id == True
            if id:
                ura = dataclasses.replace(
                    ura,
                    id=e.getparent()
                    .attrib["id"]
                    .replace("ednevnik-seznam_ur_teden-", ""),
                )

            # Vrsta ure (nadomeščanje, dogodek, odpladla...)
            imgs = e.cssselect("img")
            ura = dataclasses.replace(
                ura, vrsta=imgs[0].attrib["title"] if imgs else None
            )

            # Kratica in polno ime predmeta
            predmet = e.cssselect(".text14")[0]
            p = predmet.cssselect("span")
            if p:
                ura = dataclasses.replace(
                    ura, predmet=p[0].text.strip(), predmet_polno=p[0].attrib["title"]
                )
            else:
                ura = dataclasses.replace(
                    ura,
                    predmet=predmet.text.strip(),
                    predmet_polno=predmet.text.strip(),
                )

            # Profesor in učilnica
            sub = e.cssselect(".text11")
            if sub:
                sub = sub[0].text.split(", ")
                ura = dataclasses.replace(
                    ura, profesor=sub[0].strip(), učilnica=sub[1].strip()
                )

            # TODO: Skupinske ure

            urnik.append(ura)
    return urnik


def parse_html(text: str) -> lxml.html.HtmlElement:
    text = text.split("\x1f")
    html = text[3]
    dom = lxml.html.fragment_fromstring(html, create_parent=True)
    return dom


def prenesi(
    url: str = "https://www.easistent.com/urniki/ajax_urnik",
    teden: int = util.teden(),
    šola: int = 0,
    profesor: int = 0,
    razred: int = 0,
    dijak: int = 0,
    učilnica: int = 0,
) -> str:
    data = {
        "id_sola": šola,
        "teden": teden,
        "id_profesor": profesor,
        "id_razred": razred,
        "id_dijak": dijak,
        "id_ucilnica": učilnica,
        "qversion": 1,
    }
    r = requests.post(url, data=data)
    return r.text


def parse(html: str, id: bool = False) -> List[Ura]:
    dom = parse_html(html)
    ure = get_ure(dom)
    urnik = get_urnik(dom, ure, id=id)
    return urnik


def get(
    url: str = "https://www.easistent.com/urniki/ajax_urnik",
    teden: int = util.teden(),
    šola: int = 0,
    profesor: int = 0,
    razred: int = 0,
    dijak: int = 0,
    učilnica: int = 0,
) -> List[Ura]:
    # TODO: Urnik za več tednov
    html = prenesi(
        url=url,
        teden=teden,
        šola=šola,
        profesor=profesor,
        razred=razred,
        dijak=dijak,
        učilnica=učilnica,
    )
    urnik = parse(html)
    return urnik
