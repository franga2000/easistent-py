import re
from dataclasses import dataclass
from typing import Optional

import lxml.html
import requests

BASE_URL = "https://www.easistent.com/urniki/"

re_urnik_id_url = re.compile(
    r"(?:(?:https?:\/\/)?(?:www\.)?easistent\.com\/urniki\/)?(?P<koda>[0-9a-f]{40})"
)
re_šola_id = re.compile(r".+?var id_sola = '(\d+)';")


@dataclass
class Šola:
    ime: str
    id: Optional[int]
    razredi: dict

    def __getitem__(cls, x):
        return getattr(cls, x)


def extract_urnik_id_from_url(url: str) -> Optional[str]:
    match = re.search(re_urnik_id_url, url)

    if match is None:
        return None

    return match.group("koda")


def get_šola(šola: str) -> Šola:
    urnik_id = extract_urnik_id_from_url(šola)
    html = requests.get(BASE_URL + urnik_id).text
    dom = lxml.html.document_fromstring(html)

    razredi = {}
    for el in dom.cssselect("#id_parameter option"):
        razredi[el.text] = int(el.attrib["value"])

    šola_ime = dom.cssselect("#okvir_prijava h1")[0].text_content()
    scripts = "\n".join([s.text_content() for s in dom.cssselect("script")])

    šola_id = int(re.findall(re_šola_id, scripts)[0])

    return Šola(ime=šola_ime, razredi=razredi, id=šola_id)
