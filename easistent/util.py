"""
Pomožne funkcije
"""
from datetime import date, datetime, timedelta
from pytz import timezone


def začetek_šole(datum: date) -> date:
    """Vrne datum začetka šolskega leta, v katerem je dan datum"""
    leto = datum.year - 1 if datum.month < 9 else datum.year
    začetek = date(leto, 9, 1)
    return začetek


def teden(datum: date = date.today()) -> int:
    """Vrne številko tedna, v katerem je dan datum"""
    začetek = začetek_šole(datum)
    ponedeljek = začetek - timedelta(days=začetek.weekday())
    dnevi = (datum - ponedeljek).days
    teden = dnevi / 7
    teden = int(teden) + 1
    return teden


def preberi_čas(str: str) -> datetime:
    """Vrne datum in čas, ki ga predstavlja dano besedilo."""
    return datetime.strptime(str, "%Y-%m-%d %H:%M").replace(
        tzinfo=timezone("Europe/Ljubljana")
    )
