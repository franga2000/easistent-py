eAsistent Python knjižnica
===
[![build status](https://gitlab.com/franga2000/easistent-py/badges/master/build.svg)](https://gitlab.com/franga2000/easistent-py/commits/master) [![coverage report](https://gitlab.com/franga2000/easistent-py/badges/master/coverage.svg)](https://gitlab.com/franga2000/easistent-py/commits/master)

Neuradna knjižnica za delo s sistemom eAsistent.

## Namestitev
```bash
 $ pip install git+https://gitlab.com/franga2000/easistent-py.git
```

## Trenutne funkcije:

 - Prenos urnika za razred in dijaka z znanimi ID-ji:
	 - Predmet, Čas, Profesor, Učilnica
     - Vse vrste ur: dogodki, nadomeščanje, zaposlitve...

## Planirane funkcije:

 - Prenos urnika za celotno šolo
 - Pretvorba `/urniki/` URL-jev
 - **Podpora za skupinske ure**
 - Združevanje zaporednih ur
 - Prenos seznama nadomeščanj

## Primeri
### Urnik
```python
from easistent import urnik as eurnik
# Prenese urnik za trenutni teden
urnik = eurnik.get(šola=438, razred=174659)
```
Rezultat:
```
[{
  'predmet': 'FIZt',
  'predmet_polno': 'Fizika - Teorija',
  'profesor': 'A. Soršak',
  'učilnica': 'L08',
  'začetek': datetime.datetime(2016, 10, 18, 7, 0, tzinfo=<DstTzInfo 'Europe/Ljubljana' LMT+1:22:00 STD>),
  'konec': datetime.datetime(2016, 10, 18, 7, 45, tzinfo=<DstTzInfo 'Europe/Ljubljana' LMT+1:22:00 STD>),
  'vrsta': None
 }, ...]
```

## Dodatne informacije
 - Knjižnica **ni kompatibilna** z Pythonom 2
 - Knjižnica je pod [GPLv2](LICENSE) licenco
 - Za prenos urnikov se uporablja knjižnica [requests](https://github.com/kennethreitz/requests)
 - HTML koda je pretvorjena s knjižnico [lxml](https://github.com/lxml/lxml)
 - Urniki se prenašajo preko [nedokumentiranega API-ja](#TODO:wiki_link), ki se lahko vsak čas pokvari
