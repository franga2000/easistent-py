import json
from lxml.html import HtmlElement
from unittest import TestCase, main
from datetime import date, datetime
from pytz import timezone
from easistent import util
from easistent import urnik as eurnik


class UtilTest(TestCase):
	"""Pomožne funkcije"""

	def test_prvi_dan(self):
		"""Izračun začetka šolskega leta"""
		dt = date(year=2016, month=10, day=28)
		self.assertEqual(util.začetek_šole(dt), date(year=2016, month=9, day=1))
		dt = date(year=2016, month=3, day=19)
		self.assertEqual(util.začetek_šole(dt), date(year=2015, month=9, day=1))

	def test_teden(self):
		"""Izračun tedna v šolskem letu"""
		dt = date(year=2016, month=9, day=1)
		self.assertEqual(util.teden(dt), 1)

	def test_ura(self):
		"""Pretvorba datuma in časa"""
		dt = datetime(year=2016, month=3, day=19, hour=4, minute=20, tzinfo=timezone("Europe/Ljubljana"))
		self.assertEqual(util.preberi_čas("2016-03-19 04:20"), dt)


with open("test_data/urnik.html") as f:
	global urnik_html
	urnik_html = f.read()


class UrnikParsingTest(TestCase):
	"""Prebiranje urnika"""

	def test_parse(self):
		"""Prebiranje urnika"""
		UrnikParsingTest.dom = eurnik.parse_html(urnik_html)
		self.assertIsInstance(self.dom, HtmlElement)

	def test_ure(self):
		"""Prebiranje časa šolskih ur"""
		with open("test_data/ure.json") as f:
			ure1 = json.loads(f.read())
		ure2 = eurnik.get_ure(self.dom)
		self.assertEqual(ure1, ure2)


class UrnikValidityTest(TestCase):
	urnik = None
	"""Veljavnost prebranega urnika"""

	def setUpClass():
		UrnikValidityTest.urnik = eurnik.parse(urnik_html, id=True)

	def test_navadne(self):
		"""Lastnosti navadne ure"""
		ura = next(u for u in self.urnik if u.id == "td-3-2016-10-05")

		self.assertEqual(ura.predmet, "SLO")
		self.assertEqual(ura.predmet_polno, "Slovenščina")
		self.assertEqual(ura.vrsta, None)
		self.assertEqual(ura.profesor, "F. Prešeren")
		self.assertEqual(ura.učilnica, "L11")

	def test_skupine(self):
		"""Skupinske ure"""
		ura1 = next(u for u in self.urnik if u.id == "td-4-2016-10-06")
		ura2 = next(u for u in self.urnik if u.id == "blok-84575-2016-09-14")

		self.assertEqual(ura1.začetek, ura2.začetek)
		self.assertEqual(ura1.konec, ura2.konec)

	def test_počitnice(self):
		"""Počitnice/prazniki"""
		self.skipTest("TODO: Test za počitnice/praznike")

	def test_vrste(self):
		"""Vrste ur"""
		navadna = next(u for u in self.urnik if u.id == "td-2-2016-10-06")
		nadomescanje = next(u for u in self.urnik if u.id == "td-3-2016-10-06")
		self.assertEqual(navadna.vrsta, None)
		self.assertEqual(nadomescanje.vrsta, "Nadomeščanje")


if __name__ == "__main__":
	main()
